FROM openjdk:17-jdk-alpine

COPY target/littleback-0.0.1-SNAPSHOT.jar littleback-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-jar","/littleback-0.0.1-SNAPSHOT.jar"]
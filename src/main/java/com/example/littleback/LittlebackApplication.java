package com.example.littleback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LittlebackApplication {

	public static void main(String[] args) {
		SpringApplication.run(LittlebackApplication.class, args);
	}

}

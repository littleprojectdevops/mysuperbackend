package com.example.littleback;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.littleback.entities.User;
import com.example.littleback.repositories.UserRepo;

@Configuration
public class DataInit {
  @Bean
  CommandLineRunner initDatabase(UserRepo utilisateurRepository) {
      return args -> {
          utilisateurRepository.save(new User(null, "Dupont", "Jean", "Un passionné de la nature."));
          utilisateurRepository.save(new User(null, "Doe", "John", "Un amateur de musique."));
          utilisateurRepository.save(new User(null, "Durand", "Marie", "Spécialiste en informatique."));
      };
  }
}

package com.example.littleback.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.littleback.entities.User;
import com.example.littleback.repositories.UserRepo;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    public Optional<User> getUserById(Long id) {
        return userRepo.findById(id);
    }

    public User saveUser(User User) {
        return userRepo.save(User);
    }

    public void deleteUser(Long id) {
        userRepo.deleteById(id);
    }

    public User updateUser(Long id, User UserDetails) {
        User User = userRepo.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
        User.setNom(UserDetails.getNom());
        User.setPrenom(UserDetails.getPrenom());
        User.setDescription(UserDetails.getDescription());
        return userRepo.save(User);
    }
}

package com.example.littleback.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.littleback.entities.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
    // Ici, tu peux définir des méthodes personnalisées, par exemple :
    // List<Utilisateur> findByNom(String nom);
}

